package it.unimi.di.sweng.lab02;

import static org.assertj.core.api.Assertions.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

public class BowlingTest {

	@Rule
    public Timeout globalTimeout = Timeout.seconds(2);

	private Bowling game;

	@Before
	public void setUp(){
		game = new BowlingGame();
	}
	
	private void rollMany(int rolls, int pins) {
		for(int i=0; i<rolls; i++)
			game.roll(pins);
	}
	
	@Test
	public void gutterGame() {
		rollMany(20,0);
		assertThat(game.score()).isEqualTo(0);
	}
	
	@Test
	public void allOnesGame() {
		rollMany(20,1);
		assertThat(game.score()).isEqualTo(20);
	}
	
	@Test
	public void oneSpareGame() {
		game.roll(5);
		game.roll(5);
		game.roll(3);
		rollMany(17,0);
		assertThat(game.score()).isEqualTo(16);
	}
	
	@Test
	public void notSpareGame() {
		game.roll(1);
		game.roll(5);
		game.roll(5);
		rollMany(17,0);
		assertThat(game.score()).isEqualTo(11);
	}
	
	@Test
	public void oneStrikeGame() {
		game.roll(10);
		game.roll(3);
		game.roll(4);
		rollMany(16,0);
		assertThat(game.score()).isEqualTo(24);
	}
	
	@Test
	public void notStrikeGame() {
		 // Il test deve simulare una partita in cui vengono colpiti 10 birilli con il secondo tiro di un frame,
		 // Questa condizione deve essere riconosciuta come spare e non come strike.
		 // Es., lo score di: roll(0), roll(10), roll(3), roll(0), ..., roll(0), � 16.
		game.roll(0);
		game.roll(10);
		game.roll(3);
		assertThat(game.score()).isEqualTo(16);
	}
	
	@Test
	public void lastFrameStrikeGame() {
		 // Il test deve simulare una partita in cui avviene uno strike nell'ultimo frame.
		 // In questo caso il giocatore completa il frame ed ha diritto ad un tiro aggiuntivo.
		 // Es., lo score di: roll(0), ..., roll(0), roll(10), roll(3), roll(2), � 15.
		rollMany(18,0);
		game.roll(10);
		game.roll(3);
		game.roll(2);
		assertThat(game.score()).isEqualTo(20);
	}
	
	@Test
	public void perfectGame() { 
		 // Il test deve simulare una partita perfetta in cui avvengono 12 strike di seguito.
		 // Es., lo score di: roll(10), ..., roll(10), � 300.
		rollMany(20,10);
		assertThat(game.score()).isEqualTo(300);
	}

}
