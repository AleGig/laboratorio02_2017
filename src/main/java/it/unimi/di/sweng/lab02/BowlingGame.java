package it.unimi.di.sweng.lab02;

public class BowlingGame implements Bowling {

	private static final int MAX_ROLLS = 21;
	private int[] rolls = new int[MAX_ROLLS];
	private int currentRoll = 0;
	
	
	@Override
	public void roll(int pins) {
		if(currentRoll < MAX_ROLLS)
			rolls[currentRoll++] = pins;
	}

	@Override
	public int score() {
		
		int score = 0;
		
		for(int roll = 0; roll< MAX_ROLLS; roll++){
			
			if(isStrike(roll)){
				rolls[roll+1] = rolls[roll+1]*2;
				rolls[roll+2] = rolls[roll+2]*2;
			}
			
			if(isSpare(roll)){
				rolls[roll+2] = rolls[roll+2]*2;
			}

			score += rolls[roll];
		}
		
		return score;
	}

	private boolean isStrike(int roll) {
		return rolls[roll] == 10 && roll%2 == 0;
	}

	private boolean isSpare(int roll) {
		return roll<20 && roll % 2 == 0 && (rolls[roll] + rolls[roll+1] == 10);
	}

}
